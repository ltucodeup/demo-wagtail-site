# Demo Wagtail Site

A demo wagtail site with a simple blog.

## Installation

+ Clone this repository
+ Move into the top level of the repository

		cd demo-wagtail-site
+ Make and activate a python virtual environment (assumes python version 3.5 or above)
	+ Linux/Mac

			python3 -m venv .
			source bin/activate
	+ Windows

			python3 -m venv .
			.\Scripts\activate.bat
+ Move into demosite directory

		cd demosite
+ Install dependencies:

		pip install -r demosite/requirements.txt
+ Migrate and run server

		python manage.py migrate
		python manage.py runserver
+ Access the site at [localhost:8000](http://127.0.0.1:8000)

## Access admin panel

+ To access the admin interface, visit [localhost:8000/admin](http://127.0.0.1:8000/admin)
+ Log in with the username **testuser** and password **wagtaildabbler**
